# Stage 1: Base pour le développement
FROM dunglas/frankenphp:php8.2 AS development

WORKDIR /app

# Installer les dépendances nécessaires pour le développement
RUN apt-get update && apt-get install -y --no-install-recommends \
    adduser \
    acl \
    bash \
    build-essential \
    ca-certificates \
    curl \
    g++ \
    git \
    gnupg \
    libfreetype6-dev \
    libicu-dev \
    libjpeg-dev \
    libpng-dev \
    libpq-dev \
    libzip-dev \
    locales \
    nodejs \
    npm \
    unzip \
    vim \
    wget \
    yarn \
    zip \
    zlib1g-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install \
        pdo \
        pdo_pgsql \
        pdo_mysql \
        gd \
        intl \
        zip \
        opcache \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Installer Symfony CLI
RUN wget https://get.symfony.com/cli/installer -O - | bash \
    && mv /root/.symfony*/bin/symfony /usr/local/bin/symfony

# Installer Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Stage 2: Build pour la production
FROM development AS build

WORKDIR /app

# Copier les fichiers source dans le conteneur
COPY . /app

# Assurez-vous que les permissions sont correctes
RUN chown -R www-data:www-data /app

# Ajouter un utilisateur non-root
RUN addgroup --system appgroup && adduser --system --ingroup appgroup appuser

# Passer à l'utilisateur non-root
USER appuser

# Installer les dépendances pour la production
COPY .env /app/.env

# Stage 3: Image finale pour la production
FROM dunglas/frankenphp:php8.2 AS production

WORKDIR /app

# Copier les fichiers générés depuis l'étape de build
COPY --from=build /app /app
COPY entrypoint.sh /usr/local/bin/entrypoint.sh

# Rendre le script d'entrée exécutable
RUN chmod +x /usr/local/bin/entrypoint.sh

# Ajouter les métadonnées Git pour le suivi des builds
ARG GIT_COMMIT=unknown
ENV GIT_COMMIT=$GIT_COMMIT

# Exposer les ports nécessaires
EXPOSE 80 443

# Entrypoint pour démarrer le conteneur
ENTRYPOINT ["entrypoint.sh"]

# Commande par défaut
CMD ["frankenphp", "run", "--config=/app/frankenphp.json"]
