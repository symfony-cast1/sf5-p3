#!/bin/bash

set -e

if [ ! -d "vendor" ]; then
  echo "Installing dependencies..."
  composer install --no-dev --optimize-autoloader --no-interaction
fi

if [ "$APP_ENV" != "prod" ]; then
  echo "Running migrations..."
  php bin/console doctrine:migrations:migrate --no-interaction || true
fi

echo "Clearing and warming up cache..."
php bin/console cache:clear || true
php bin/console cache:warmup || true

exec frankenphp run --config=/app/frankenphp.json "$@"
